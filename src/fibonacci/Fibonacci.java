package fibonacci;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fibonacci {

	
	/**
	 * Given an array of ints, this method returns a sorted List that contains
	 * the Fibonacci numbers in the input list. The list returned contains the
	 * Fibonacci numbers in ascending order and has no duplicates.
	 * 
	 * @param inputArray
	 * @return a list of all the Fibonacci numbers in inputArray, in ascending
	 *         order, with no duplicates. If the input array is null or does not
	 *         contain any Fibonacci numbers then an empty list is returned.
	 */
	public static List<Integer> getFibonacciNumbers_sorted(Integer[] inputArray) {
		
		ArrayList<Integer> fibnum = new ArrayList<Integer>();
		if(inputArray==null){
			return fibnum;
		}
		for(int i=0;i<inputArray.length;i++){
			if(isFib(inputArray[i])){
				if(!fibnum.contains(inputArray[i])){
				fibnum.add(inputArray[i]);
				}
			}
		}
		Collections.sort(fibnum);	
		return fibnum; 
	}
	
	public static boolean isFib(int n){
		boolean isFibs;
		if(n<=0){
			return false;
		}
		    
		 isFibs=(isSquare(5*n*n + 4) ||isSquare(5*n*n - 4));
		
		return isFibs;
		
	}
	
	public static boolean isSquare(int r){
		int s = (int) Math.sqrt(r);
	    return (s*s == r);
		
		
	}
	public static void main(String args[]){
		Integer[] inArray = {7, 8, 55, 13, 13, 20, 19, 21, 1, 21,0,-8};
        
		System.out.println(Fibonacci.getFibonacciNumbers_sorted(inArray));
		
	}
}
