package expression;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import javax.xml.stream.events.Characters;

public class FPExpression extends Expression {

	/**
	 * Constructor for FPExpression. This is a private constructor. Objects of
	 * this type can only be created by clients through a call to the static
	 * method 'getFPExpression(String)' or 'getFPExpression(Expression)'. This
	 * is to preserve the representation invariant and the sub-typing
	 * relationship.
	 * 
	 * @param expression
	 */
	private FPExpression(String expression) {
		super(expression);
	}

	/**
	 * 
	 * @param expression
	 * @return an FPExpression object with the same input expression if the
	 *         input expression is fully parenthesized.
	 * @throws IllegalArgumentException
	 *             if the input is not a valid fully parenthesized expression.
	 */
	public static FPExpression getFPExpression(String expression)
			throws IllegalArgumentException {
		if(!isFPExpression(expression)){
			throw new IllegalArgumentException("Brackets incorrect");
		}
	    
		// TODO: Implement this method

		// If the input String is a valid fully parenthesized expression then
		// the following statement is correct.
		// This uses the private constructor.
		return new FPExpression(expression);
	}

	/**
	 * 
	 * @param expression
	 * @return an FPExpression object with the input expression string if the
	 *         input expression is fully parenthesized. 
	 * @throws IllegalArgumentException
	 *             if the input is not a valid fully parenthesized expression.
	 */
	public static FPExpression getFPExpression(Expression expression)
			throws IllegalArgumentException {
		// TODO: Implement this method. Somewhat similar to the method above.

		return null; // change this for sure!

	}

	/**
	 * Given an Expression, check if the expression is a fully parenthesized
	 * expression.
	 * 
	 * @param expression
	 * @return true if expression is fully parenthesized and false otherwise
	 */
	public static boolean isFPExpression(Expression expression) {
		// This method is okay. Don't have to change this.
		return isFPExpression(expression.toString());
	}

	/**
	 * Given a String check if the string represents a fully parenthesized
	 * expression.
	 * 
	 * @param expression
	 * @return true if expression is fully parenthesized and false otherwise
	 */
	public static boolean isFPExpression(String expression) {
		ArrayList<Integer> br1 = new ArrayList<Integer>();
		ArrayList<Integer> br2 = new ArrayList<Integer>();
		ArrayList<Integer> br3 = new ArrayList<Integer>();
		
		if(expression==""){
			return false;
		}
		
		ArrayList<Character> brackets = new ArrayList<Character>();
		for (int i = 0; i < expression.length(); i++) {
			if(expression.charAt(i)=='('){
			  brackets.add(expression.charAt(i));
			  br1.add(10);
			}
			if(expression.charAt(i)=='['){
				  brackets.add(expression.charAt(i));
				  br2.add(10);
			}
			if(expression.charAt(i)=='{'){
				  brackets.add(expression.charAt(i));
				  br3.add(10);
			}
			if(expression.charAt(i)==')'){
				  brackets.add(expression.charAt(i));
				  br1.add(20);
			}
			if(expression.charAt(i)==']'){
					  brackets.add(expression.charAt(i));
					  br2.add(20);
			}
			if(expression.charAt(i)=='}'){
					  brackets.add(expression.charAt(i));
					  br3.add(20);
				}
			}
		System.out.println(brackets);
		int openbracket1 = 0;
		int openbracket2 = 0;
		int openbracket3 = 0;
		int closebracket1 = 0;
		int closebracket2 = 0;
		int closebracket3 = 0;
		
		for(int i=0;i<brackets.size();i++){
			if(brackets.get(i)=='(')
				 openbracket1++;
				if(brackets.get(i)=='[')
					  openbracket2++;
				if(brackets.get(i)=='{')
					openbracket3++;
				if(brackets.get(i)==')')
					 closebracket1++;
					if(brackets.get(i)==']')
						  closebracket2++;
					if(brackets.get(i)=='}')
						closebracket3++;
		}
		if((openbracket1==closebracket1) && (openbracket2==closebracket2)&& (openbracket3==closebracket3)){
			for(int i=0;i<br1.size();i++){
				if(br1.get(i)==10+br1.get(br1.size()-i-1));
				else break;
			}
			for(int i=0;i<br2.size();i++){
				if(br2.get(i)==10+br2.get(br2.size()-i-1));
				else break;
			}
			for(int i=0;i<br3.size();i++){
				if(br3.get(i)==10+br3.get(br3.size()-i-1));
				else break;
			}
			return true;
		}
		System.out.println(openbracket1);
		System.out.println(openbracket2);
		System.out.println(closebracket1);
		System.out.println(br1);
		System.out.println(br2);
		System.out.println(br3);
		return false; // change this
	}
	
	public static void main (String [] args){
		boolean r = FPExpression.isFPExpression("(abc)");
		System.out.println(r);
	}

}
